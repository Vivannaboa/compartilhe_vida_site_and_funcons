const functions = require('firebase-functions');
const admin = require('firebase-admin');

var serviceAccount = require("./compartilhevida-6fce1-firebase-adminsdk-28kax-8c75d92e36.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://compartilhevida-6fce1.firebaseio.com"
});

const ref = admin.database().ref();

exports.sendNotification = functions.database.ref("Notifications/{uid}").onWrite(event => {
    var request = event.data.val();
    var payload = {
        data: {
            titulo: request.titulo,
            mensagem: request.mensagem,
        }
    };

    admin.messaging().sendToCondition(request.topico, payload)
        .then(function (response) {
            event.data.ref.remove();
            console.log("Successfully sent message: ", response);
        })
        .catch(function (error) {
            console.log("Error sending message: ", error);
        })
})

exports.criaNotificacao = functions.https.onRequest((req, res) => {
    const currentTime = new Date()
    console.log("Passou na funcao");
    ref.child("doacoes").once("value", function (snepshot) {
        snepshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
                if((new Date(childData.proxima_doacao).toLocaleDateString()) == currentTime.toLocaleDateString()){
                 var payload = {
                    data: {
                        titulo: "Você já pode doar sangue novamente!",
                        mensagem:  "Hoje você ja pode doar sangue novamente."
                    }
                };
                 admin.messaging().sendToDevice(childData.uid_user, payload)
                 .then(function (response) {
                     console.log("Successfully sent message: ", response);
                 })
                 .catch(function (error) {
                     console.log("Error sending message: ", error);
                 })
            }            
           

           
        })
       
    })
    res.status(200).send("ok");
})